//#region PREAMBLE
/*
    This is a libre drawing game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

type SignalListener<ArgumentTypes extends any[]> = (...a:ArgumentTypes)=>void;

export class Signal<ArgumentTypes extends any[]> {
    private listeners = new Set<(...a:ArgumentTypes)=>void>();

    public connect(listener: SignalListener<ArgumentTypes>) {
        this.listeners.add(listener);
    }

    public disconnect(listener: SignalListener<ArgumentTypes>) {
        this.listeners.delete(listener);
    }

    public emitSignal(...args : ArgumentTypes) {
        this.listeners.forEach(listener => listener(...args));
    }
}
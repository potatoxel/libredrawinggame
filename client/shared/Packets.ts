//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

export interface ReceivedData {
    type: string;
    json: object;
}

export interface PacketCreateJoinRoom {
    nickname: string;
    roomid: string;
}

export interface PacketAlert {
    message: string;
}

export interface PacketSwitchScene {
    newScene: string;
}

export interface PacketPlayerList {
    players: Array<{
        name: string;
        status: string;
    }>
}

export interface PacketPlayerReady {
    ready: boolean;
}

export interface PacketSendPrompt {
    prompt: string;
}

export interface PacketSendImage {
    image: string;
}

export interface PacketOptions {
    options: Array<{
        name: string;
        suboptions: Array<string>;
    }>;
}
//#region PREAMBLE
/*
    This is a libre drawing game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion
import { DrawingCanvas } from "./DrawingCanvas.js";
import { PacketAlert, PacketCreateJoinRoom, PacketPlayerList, PacketOptions, PacketPlayerReady, PacketSendImage, PacketSendPrompt, PacketSwitchScene } from "./shared/Packets.js";
import { Socket } from "./Socket.js";

const canvas = document.getElementById("drawingCanvas") as HTMLCanvasElement;
const drawingCanvas = new DrawingCanvas(canvas);

const socket: Socket = new Socket();
socket.connect((window.location.protocol.endsWith("s:") ? "wss" : "ws") + "://" + window.location.host + "/server");

const screenMain = document.getElementById("screenMain");
const screenRoomLobby = document.getElementById("screenRoomLobby");
let currentScene = screenMain;

socket.on("alert", (data: PacketAlert) => {
    alert(data.message);
})

socket.on("changeScene", (data: PacketSwitchScene) => {
    currentScene.classList.add("hidden");    
    currentScene = document.getElementById(data.newScene);
    currentScene.classList.remove("hidden");
})

socket.on("playerList", (data: PacketPlayerList) => {
    console.log("Updating playerlist");
    let playerListHTML = document.createElement("div");
    console.log(data);
    data.players.forEach((player) => {
        let playerHTML = document.createElement("p");
        playerHTML.innerText = player.name + " " + player.status;
        playerListHTML.appendChild(playerHTML);
    })
    let playerLists = document.getElementsByClassName("playerList");
    for(let i = 0; i < playerLists.length; i++) {
        let playerList = playerLists[i];
        playerList.innerHTML = "";
        playerList.appendChild(playerListHTML.cloneNode(true));
    }
});

socket.on("sendImage", (data: PacketSendImage) => {
    let canvas: HTMLCanvasElement;
    if(currentScene.id == "screenViewingGame") {
        canvas = document.getElementById("viewingCanvas") as HTMLCanvasElement;
    } else {
        canvas = document.getElementById("guessingCanvas") as HTMLCanvasElement;
    }
    const context = canvas.getContext("2d");
    const img = document.createElement("img");
    img.src = data.image;
    img.onload = () => {
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(img, 0, 0);
    }
});

socket.on("sendPrompt", (data: PacketSendPrompt) => {
    let labels = document.getElementsByClassName("promptLabel")
    for(let i = 0; i < labels.length; i++) {
        let label = labels[i];
        (label as HTMLDivElement).innerText = data.prompt;
    }
});

socket.on("optionsList", (data: PacketOptions) => { 
    let div = document.getElementById("optionsList") as HTMLDivElement;
    div.innerHTML = "";
    data.options.forEach(player => {
        let p = document.createElement("p");
        p.innerText = player.name;
        player.suboptions.forEach((somethingToView) => {
            let button = document.createElement("button");
            button.innerText = somethingToView;
            button.onclick = () => {
                socket.emit("sendImage", {
                    image: player.name + "," + somethingToView
                } as PacketSendImage)
            }
            p.appendChild(button);
        });
        div.appendChild(p);
    });
});

socket.on("clearDrawingCanvas", () => {
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
});

document.getElementById("createRoomButton").onclick = () => {
    const nicknameText = document.getElementById("nickname") as HTMLInputElement;
    const roomIdText = document.getElementById("roomId") as HTMLInputElement;
    socket.emit("createRoom", {
        nickname: nicknameText.value,
        roomid: roomIdText.value
    } as PacketCreateJoinRoom)
}

document.getElementById("joinRoomButton").onclick = () => {
    const nicknameText = document.getElementById("nickname") as HTMLInputElement;
    const roomIdText = document.getElementById("roomId") as HTMLInputElement;
    socket.emit("joinRoom", {
        nickname: nicknameText.value,
        roomid: roomIdText.value
    } as PacketCreateJoinRoom)
}

document.getElementById("startGameButton").onclick = () => {
    socket.emit("playerReady", {
        ready: true
    } as PacketPlayerReady);
}

document.getElementById("doneDrawingButton").onclick = () => {
    socket.emit("sendImage", { 
        image: canvas.toDataURL()
    } as PacketSendImage);
}

document.getElementById("donePromptButton").onclick = () => {
    socket.emit("sendImage", {
        image: (document.getElementById("promptText") as HTMLInputElement).value
    } as PacketSendImage);    
}

document.getElementById("doneGuessingButton").onclick = () => {
    socket.emit("sendImage", {
        image: (document.getElementById("guessingText") as HTMLInputElement).value
    } as PacketSendImage);
}



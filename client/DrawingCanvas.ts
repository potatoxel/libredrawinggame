//#region PREAMBLE
/*
    This is a libre drawing game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

export class DrawingCanvas {
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    mouseDown: boolean = false;

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;

        this.canvas.addEventListener("mousedown", (ev) => this.startDrawing(this.getPos({x: ev.clientX, y: ev.clientY})));
        this.canvas.addEventListener("mousemove", (ev) => this.draw(this.getPos({x: ev.clientX, y: ev.clientY})));
        this.canvas.addEventListener("mouseup", (ev) => this.stopDrawing(this.getPos({x: ev.clientX, y: ev.clientY})));
        this.context = this.canvas.getContext('2d');
    }

    startDrawing(position: { x: number; y: number; }): any {
        this.mouseDown = true;
    }

    stopDrawing(position: { x: number; y: number; }): any {
        this.mouseDown = false;
    }

    draw(position: { x: number; y: number; }): any {
        if(this.mouseDown) {
            this.context.fillStyle = "#f00ff0";
            this.context.fillRect(position.x, position.y, 4, 4);
        }
    }
    
    getPos(position: {x: number, y: number}) {
        var rect = this.canvas.getBoundingClientRect();
        return {
            x: position.x - rect.left,
            y: position.y - rect.top
        }
    }
}
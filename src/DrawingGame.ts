//#region PREAMBLE
/*
    This is a libre drawing game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { PacketAlert, PacketOptions, PacketSendImage, PacketSendPrompt, PacketSwitchScene } from "../client/shared/Packets";
import { ClientHandler } from "./ClientHandler";
import { Room } from "./Room";
import { Server } from "./Server";

export class GamePlayer {  
    public clientHandler: ClientHandler;
    public drawing: string;

    public isDone() {
        return this.drawing != null;
    }

    public getStatus() {
        return "done: " + this.isDone()
    }

    public clone(): GamePlayer {
        const player = new GamePlayer();
        player.clientHandler = this.clientHandler;
        player.drawing = this.drawing;
        return player;
    }
}

export class GameBaseBase {
    protected gameInProgress: boolean = false;
    protected room: Room;
    protected players = new Map<ClientHandler, GamePlayer>();
    protected playerList = new Array<GamePlayer>();
    protected completeGame: () => void = () => null;

    public screen: string;

    constructor(room: Room) {
        this.room = room;
    }

    addClient(client: ClientHandler) {
        const gamePlayer = new GamePlayer();
        gamePlayer.clientHandler = client;
        this.addPlayer(gamePlayer);
    }

    addPlayer(player: GamePlayer) {
        let clone = player.clone();
        this.players.set(player.clientHandler, clone);
        this.playerList.push(clone);
    }

    removePlayer(player: GamePlayer) {
        this.players.delete(player.clientHandler);
    }

    removeClient(client: ClientHandler) {
        this.players.delete(client);
    }

    submitDrawing(client: ClientHandler, drawing: string) {

    }

    isGameDone() {
        let res = true;
        this.players.forEach((player) => {
            if(!player.isDone()) res = false;
        });
        return res;
    }

    getClientStatus(client: ClientHandler) {
        return this.players.get(client).getStatus();
    }

    async start() {

    }

    getPlayer(clientHandler: ClientHandler): GamePlayer {
        return this.players.get(clientHandler);
    }

    addAllPlayersFrom(game: GameBaseBase) {
        game.playerList.forEach(player => {
            this.addPlayer(player);
        })
    }

}

export class GameBase<T> extends GameBaseBase{
    getCompletionData(): T {
        return null;
    }

    waitUntilCompleted(): Promise<T> {
        return new Promise<T>((executor, reject) => {
            this.completeGame = () => { 
                executor(this.getCompletionData()); 
            };
        });
    }
}

export class PromptingGame extends GameBase<Array<string>> {
    screen = "screenPromptGame";

    submitDrawing(client: ClientHandler, drawing: string) {
        this.players.get(client).drawing = drawing;
        this.room.updatePlayerList();
        
        if(this.isGameDone()) {
            this.completeGame();
        }
    }

    getCompletionData() {
        let res = new Array<string>();
        this.playerList.forEach(player => {
            res.push(player.drawing);
        });
        return res;
    }
}

export class DrawingGame extends GameBase<Array<string>> {
    screen = "screenDrawingGame"
    prompts: Array<string>;

    constructor(room: Room, prompts: Array<string>) {
        super(room);
        this.prompts = prompts;
    }

    async start() {
        for(let i = 0; i < this.playerList.length; i++) {
            let player = this.playerList[i];
            player.clientHandler.emit("sendPrompt", {
                prompt: "Draw: " + this.prompts[i]
            } as PacketSendPrompt);
        }
    }

    submitDrawing(client: ClientHandler, drawing: string) {
        this.players.get(client).drawing = drawing;
        this.room.updatePlayerList();      
        
        if(this.isGameDone()) {
            this.completeGame();
        }
    }

    
    getCompletionData() {
        let res = new Array<string>();
        this.playerList.forEach(player => {
            res.push(player.drawing);
        });
        return res;
    }
}

export class GuessingGame extends GameBase<Array<string>> {
    screen = "screenGuessingGame"
    drawings: Array<String>;

    constructor(room: Room, drawings: Array<String>) {
        super(room);
        this.drawings = drawings;
    }

    async start() {
        for(let i = 0; i < this.playerList.length; i++) {
            let player = this.playerList[i];
            player.clientHandler.emit("sendImage", {
                image: this.drawings[i]
            } as PacketSendImage);
        }

        this.playerList.forEach(player => {
            player.drawing = null;
        });
    }

    submitDrawing(client: ClientHandler, drawing: string) {
        this.players.get(client).drawing = drawing;
        this.room.updatePlayerList();
        
        if(this.isGameDone()) {
            this.completeGame();
        }       
    }

    getCompletionData() {
        let res = new Array<string>();
        this.playerList.forEach(player => {
            res.push(player.drawing);
        });
        return res;
    }
}

type Drawing = {
    prompt: string;
    drawing: string;
    guess: string;
};

type GameInformation = Map<string, Array<Drawing>>;

export class ViewingGame extends GameBase<void> {
    screen = "screenViewingGame"
    gameInformation: GameInformation;

    constructor(room: Room, gameInformation: GameInformation) {
        super(room);
        this.gameInformation = gameInformation;
    }

    async start() {
        let res: PacketOptions = {
            options: []
        };
        
        this.gameInformation.forEach((value, key) => {
            let arr: string[] = [];
            for(let i = 0; i < value.length; i++) {
                arr.push(i.toString());
            }

            res.options.push({ 
                name: key,
                suboptions: arr
            })
        });

        this.room.broadcast("optionsList", res);
    }

    show(prompt: string, id: number) {
        const info = this.gameInformation.get(prompt)[id];
        this.room.broadcast("sendPrompt", {
            prompt: "Prompt: " + info.prompt + ", Guess: " + info.guess
        } as PacketSendPrompt);
        this.room.broadcast("sendImage", {
            image: info.drawing
        } as PacketSendImage);
    }

    submitDrawing(client: ClientHandler, drawing: string) {
        let sp = drawing.split(",");
        let option = sp[0];
        let pictureId = sp[1];
        this.show(option, Number.parseInt(pictureId));
    }

}

export class PromptDrawingGuessGame extends GameBase<void> {
    public currentGame: GameBaseBase;
    
    constructor(room: Room) {
        super(room);

    }

    async start() {
        const prompts = await this.doGame(new PromptingGame(this.room));
        const gameInformation: GameInformation = new Map<string, Array<Drawing>>();

        prompts.forEach((prompt) => {
            gameInformation.set(prompt, []);
        });

        let guesses = prompts;

        for(let i = 0; i < 3; i++) {
            let prompts_rotated = rotate(prompts, 2*i+1);
            let prev_guesses_rotated = rotate(guesses, 1);
            
            this.room.clearDrawingCanvas();
            const drawings = await this.doGame(new DrawingGame(this.room, prev_guesses_rotated));
            guesses = await this.doGame(new GuessingGame(this.room, rotate(drawings,1)));
            const temp_guesses = rotate(guesses, -1);
            
            prompts_rotated.forEach((prompt, index) => {
                let info: Drawing = {
                    prompt: prev_guesses_rotated[index],
                    drawing: drawings[index],
                    guess: temp_guesses[index]
                }
                gameInformation.get(prompt).push(info);
            });
        }

        await this.doGame(new ViewingGame(this.room, gameInformation));
    }

    submitDrawing(client: ClientHandler, drawing: string) {
        this.currentGame.submitDrawing(client, drawing);
    }

    async doGame<T>(game: GameBase<T>): Promise<T> {
        this.currentGame = game;
        this.room.changeScreen(this.currentGame.screen);
        game.addAllPlayersFrom(this);
        this.currentGame.start();
        return await game.waitUntilCompleted();
    }

    getClientStatus(client: ClientHandler) {
        return this.currentGame.getClientStatus(client);
    }
}


function rotate<T>(array: T[], amount: number): T[] {
    let res = new Array<T>();
    res.length = array.length;
    for(let i = 0; i < array.length; i++) {
        res[mod(i+amount, array.length)] = array[i];
    }
    return res;
}

function mod(x: number, y: number): number {
    return (x % y + y) % y;
}
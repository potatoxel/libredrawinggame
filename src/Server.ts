//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { ClientHandler } from './ClientHandler.js';
import { app } from './app';
import { Room } from './Room.js';

export class Server {
    private clients = new Set<ClientHandler>();
    public rooms = new Map<string, Room>();

    addClient(client: ClientHandler) {
        this.clients.add(client);
    }


    start() {
        app.ws('/server', (ws, req) => {
            var clientHandler = new ClientHandler(this, ws);
            clientHandler.initializeEvents();
            this.addClient(clientHandler);
            clientHandler.setup();
            ws.on('close', () => {
                this.clients.delete(clientHandler);
            });
        });

        console.log("Server running");
    }

    broadcast(type: string, data: any, except: Set<ClientHandler> = null) {
        this.broadcastIn(this.clients, type, data, except);
    }

    public broadcastIn(players: Set<ClientHandler>, type: string, data: any, except: Set<ClientHandler>) {
        var data_to_send = JSON.stringify({
            "type": type,
            "json": data
        });

        players.forEach((client) => {
            if (except && except.has(client))
                return;
            client.emitString(data_to_send);
        });
    }
}
